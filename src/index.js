import fs from 'fs'
import path from 'path'
import models from './db/models'

import App from './app'

// Check if DB directory exists
const dbPath = path.join(__dirname, '../db')
try {
  if (!fs.existsSync(dbPath)) {
    fs.mkdirSync(dbPath)
  }
} catch (e) {
  console.log('Unable to create DB directory')
  process.exit(1)
}

// Sync database with latest migrations (creates the database/tables if not exist)
models
  .sequelize
  .sync()
  .then(() => {
    const app = new App()
    app.start()
  })
  .catch((e) => {
    console.log('Unable to initialize the database')
    process.exit(1)
  })
