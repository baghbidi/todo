import bodyParser from 'body-parser'
import express from 'express'
import fs from 'fs'
import helmet from 'helmet'
import path from 'path'

export default class App {
  constructor() {
    this.express = express()
    this.express.deprecate = () => {}
    this.server = null
  }
  start(port = 3000) {
    // Adds these headers to response header:
    //   Strict-Transport-Security →max-age=15552000; includeSubDomains
    //   X-Content-Type-Options →nosniff
    //   X-DNS-Prefetch-Control →off
    //   X-Download-Options →noopen
    //   X-Frame-Options →SAMEORIGIN
    //   X-XSS-Protection →1; mode=block
    this.express.use(helmet())

    this.express.use(bodyParser.urlencoded({
      extended: true,
    }))
    this.express.use(bodyParser.json())

    this.express.on('uncaughtException', this.onError)

    // Registering routes
    const registerRoute = (route) => {
      this.express[route.method.toLowerCase()](route.path, route.middleware)
    }

    const setupMiddleware = (routeName) => {
      const routes = [].concat(require(path.join(__dirname, 'routes', routeName)) || [])
      routes.forEach(registerRoute)
    }

    fs.readdirSync(path.join(__dirname, 'routes'))
      .forEach(setupMiddleware)

    // Starting server
    this.server = this.express.listen(port, (err) => {
      if (err) {
        console.error(`Unable to start server on port ${port}.`)
      }

      if (process.env.NODE_ENV !== 'test') {
        console.log(`🌎  Server listening on port ${port}. Open up http://localhost:${port}/ in your browser.`)
      }
    })
  }

  stop() {
    if (this.server) {
      return new Promise((resolve, reject) => {
        this.server.close(() => resolve())
      })
    }
  }

  onError(req, res, route, err) {
    res.status(500).send(err)
  }
}