export default (sequelize, DataTypes) => {
  const List = sequelize.define('List', {
    listId: DataTypes.STRING,
    name: DataTypes.STRING,
    description: DataTypes.STRING,
  }, {})

  List.associate = (models) => {
    List.hasMany(models.Task)
  }

  return List
}