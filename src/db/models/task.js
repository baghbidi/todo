export default (sequelize, DataTypes) => {
  const Task = sequelize.define('Task', {
    taskId: DataTypes.STRING,
    name: DataTypes.STRING,
    completed: DataTypes.BOOLEAN,
  }, {})

  Task.associate = (models) => {
    Task.belongsTo(models.List, {
      onDelete: "CASCADE",
      foreignKey: {
        allowNull: false,
      },
    })
  }

  return Task
}