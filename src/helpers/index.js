export function validateId(uuid) {
  return /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i.test(uuid)
}

export function validateTask(task) {
  return validateId(task.id) &&
    task.name != null &&
    task.name.trim() != ''
}

export function validateList(list) {
  const tasks = list.tasks || []
  return validateId(list.id) &&
    list.name != null &&
    list.name.trim() != '' &&
    Array.isArray(tasks) &&
    tasks.every(validateTask) &&
    tasks.every((task, i) => tasks.findIndex(t => t.id === task.id) === i) // check for duplicate task ids
}