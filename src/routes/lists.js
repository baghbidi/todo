import { Op } from 'sequelize'

import models from '../db/models'
import * as helpers from '../helpers'

module.exports = [
  {
    method: 'get',
    path: '/lists',
    middleware: async (req, res) => {
      const params = req.query || {}
      const searchString = params.searchString || ''
      const offset = params.skip || 0
      const limit = params.limit || -1

      if (isNaN(offset) || isNaN(limit)) {
        return res.status(400).send()
      }

      try {
        const lists = await models.List.findAll({
          where: {
            [Op.or]: [
              { name: { [Op.like]: `%${searchString}%` } },
              { description: { [Op.like]: `%${searchString}%` } },
            ],
          },
          include: [models.Task],
          offset,
          limit,
        })

        res
          .status(200)
          .json(lists.map(createListJson))
      } catch (e) {
        res.status(500).send(e)
      }
    },
  },

  {
    method: 'post',
    path: '/lists',
    middleware: async (req, res) => {
      const body = req.body || {}

      if (!helpers.validateList(body)) {
        return res.status(400).send()
      }

      try {
        await models.sequelize.transaction((t) => {
          return models.List.findOrCreate({
            where: { listId: body.id },
            defaults: {
              name: body.name,
              description: body.description,
            },
          }).then(([list, created]) => {
            if (!created) {
              return res.status(409).send()
            }

            const tasks = body.tasks || []
            const promises = tasks.map((task) => {
              models.Task.create({
                taskId: task.id,
                name: task.name,
                completed: task.completed === true,
                ListId: list.id,
              })
            })
            return Promise.all(promises)
          })
        })
        res.status(201).send()
      } catch (e) {
        res.status(500).send(e)
      }
    },
  },

  {
    method: 'get',
    path: '/list/:id',
    middleware: async (req, res) => {
      const { id } = req.params

      if (!helpers.validateId(id)) {
        return res.status(400).send()
      }

      try {
        const list = await models.List.findOne({
          where: { listId: id },
          include: [models.Task],
        })

        if (list == null) {
          return res.status(404).send()
        }

        res
          .status(200)
          .json(createListJson(list))
      } catch (e) {
        res.status(500).send(e)
      }
    }
  },
]

function createListJson(list) {
  return {
    id: list.listId,
    name: list.name,
    description: list.description,
    tasks: list.Tasks.map((task) => ({
      id: task.taskId,
      name: task.name,
      completed: task.completed,
    }))
  }
}