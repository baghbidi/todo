import models from '../db/models'
import * as helpers from '../helpers'

module.exports = [
  {
    method: 'post',
    path: '/list/:id/tasks',
    middleware: async (req, res) => {
      const body = req.body || {}
      const { id } = req.params

      if (!helpers.validateId(id) || !helpers.validateTask(body)) {
        return res.status(400).send()
      }

      try {
        const list = await models.List.findOne({
          where: { listId: id },
          include: [models.Task],
        })

        if (list == null) {
          return res.status(400).send()
        }

        const [task, created] = await models.Task.findOrCreate({
          where: { taskId: body.id },
          defaults: {
            name: body.name,
            completed: body.completed === true,
            ListId: list.id,
          },
        })
        if (!created) {
          return res.status(409).send()
        }

        res.status(201).send()
      } catch (e) {
        res.status(500).send(e)
      }
    },
  },

  {
    method: 'post',
    path: '/list/:id/task/:taskId/complete',
    middleware: async (req, res) => {
      const { id, taskId } = req.params

      if (!helpers.validateId(id) || !helpers.validateId(taskId)) {
        return res.status(400).send()
      }

      try {
        const task = await models.Task.findOne({
          where: { taskId },
          include: [{ model: models.List, where: { listId: id } }],
        })
        if (task == null) {
          return res.status(400).send()
        }

        await models.Task.update(
          { completed: true },
          { where: { taskId } }
        )
        res.status(201).send()
      } catch (e) {
        res.status(500).send(e)
      }
    },
  },

]