import chai from 'chai'
import chaiAsPromised from 'chai-as-promised'
import chaiHttp from 'chai-http'

require('babel-polyfill')

process.env.NODE_ENV = 'test'

MAIN: {
    chai.use(chaiAsPromised)
    chai.use(chaiHttp)
}
