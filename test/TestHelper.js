import chai from 'chai'
import path from 'path'

const PROJ_ROOT = '../src'

const expect = chai.expect

export function _require(_path, { onlyDefault = true } = {}) {
  const filePath = path.join(PROJ_ROOT, _path)

  const obj = require(filePath)

  return (onlyDefault && obj.hasOwnProperty('default')) ? obj.default : obj
}

const lists = [
  {
    id: 'd290f1ee-6c54-4b01-90e6-d701748f0851',
    name: 'list-name-1',
    description: 'list-description-1',
    tasks: [
      {
        id: '0e2ac84f-f723-4f24-878b-44e63e7ae580',
        name: 'task-name-1',
        completed: false,
      }
    ],
  },
  {
    id: 'd290f1ee-6c54-4b01-90e6-d701748f0849',
    name: 'list-name-2',
    description: 'list-description-2',
    tasks: [
      {
        id: '0e2ac84f-f723-4f24-878b-44e63e7ae581',
        name: 'task-name-2',
        completed: false,
      }
    ],
  },
]

function seed(models) {
  return lists.forEach((list) => {
    models.List.create({
      listId: list.id,
      name: list.name,
      description: list.description,
      Tasks: [{
        taskId: list.tasks[0].id,
        name: list.tasks[0].name,
        completed: list.tasks[0].completed,
      }],
    }, {
        include: [models.Task],
      })
      .catch(e => console.log(e))
  })
}

export {
  expect,
  seed,
  lists,
}
