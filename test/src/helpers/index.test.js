import { expect, _require } from '../../TestHelper'
const helpers = _require('helpers')

describe('Helpers', () => {
  describe('.validateId()', () => {
    def('uuid', () => '0e2ac84f-f723-4f24-878b-44e63e7ae580')

    context('when provided id is not valid', () => {
      def('uuid', () => 'aaaa')

      it('returns false', () => {
        expect(helpers.validateId($uuid)).to.be.false
      })
    })

    context('when provided id is valid', () => {
      it('returns true', () => {
        expect(helpers.validateId($uuid)).to.be.true
      })
    })
  })

  describe('.validateTask()', () => {
    def('id', () => '0e2ac84f-f723-4f24-878b-44e63e7ae580')
    def('name', () => 'Test Task!')
    def('task', () => ({
      id: $id,
      name: $name,
    }))

    context('when provided task.id is not valid', () => {
      def('id', () => 'aaa')

      it('returns false', () => {
        expect(helpers.validateTask($task)).to.be.false
      })
    })

    context('when provided task.id is valid', () => {
      context('when provided task.name is null', () => {
        def('name', () => void 0)

        it('returns false', () => {
          expect(helpers.validateTask($task)).to.be.false
        })
      })

      context('when provided task.name is empty', () => {
        def('name', () => '')

        it('returns false', () => {
          expect(helpers.validateTask($task)).to.be.false
        })
      })

      context('when provided task.name is valid', () => {
        it('returns true', () => {
          expect(helpers.validateTask($task)).to.be.true
        })
      })
    })
  })
})

