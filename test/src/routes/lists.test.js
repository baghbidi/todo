import chai from 'chai'

import { expect, _require, seed, lists } from '../../TestHelper'
import App from '../../../src/app'
import models from '../../../src/db/models'

describe('List Routes', () => {
  const app = new App()
  def('server', () => chai.request(app.express))
  def('request', () => void 0)

  before(() => app.start())
  after((done) => {
    app.stop().then(done)
  })

  beforeEach((done) => {
    models.sequelize.sync({ force: true }).then(() => {
      return seed(models)
    }).then(() => done())
  })

  describe('GET /lists', (done) => {
    def('request', () => $server.get('/lists'))

    context('when no query parameter provided', () => {
      it('returns all of the available lists', (done) => {
        $request
          .end((err, res) => {
            expect(res.status).to.equal(200)
            expect(res).to.be.json
            expect(res.body).to.be.an('array').lengthOf(2)
            done()
          })
      })
    })

    context('when searchString is provided', () => {
      def('searchString', () => '-name-1')
      def('request', () => $server.get('/lists?searchString=' + $searchString))

      context('when name contains searchString', () => {
        it('filters lists by searchString', (done) => {
          $request
            .end((err, res) => {
              expect(res.status).to.equal(200)
              expect(res).to.be.json
              expect(res.body).to.be.an('array').lengthOf(1)
              done()
            })
        })
      })

      context('when name does not contain searchString', () => {
        def('searchString', () => '-description-1')

        context('when description contains searchString', () => {
          it('filters lists by searchString', (done) => {
            $request
              .end((err, res) => {
                expect(res.status).to.equal(200)
                expect(res).to.be.json
                expect(res.body).to.be.an('array').lengthOf(1)
                done()
              })
          })
        })

        context('when description does not contain searchString', () => {
          def('searchString', () => 'invalid!')

          it('returns an empty array', (done) => {
            $request
              .end((err, res) => {
                expect(res.status).to.equal(200)
                expect(res).to.be.json
                expect(res.body).to.be.an('array').lengthOf(0)
                done()
              })
          })
        })
      })
    })

    context('when skip=n is provided', () => {
      def('skip', () => 1)
      def('request', () => $server.get('/lists?skip=' + $skip))

      context('when skip is not a valid number', () => {
        def('skip', () => 'aaa')

        it('returns 400', (done) => {
          $request
            .end((err, res) => {
              expect(res.status).to.equal(400)
              done()
            })
        })
      })

      context('when skip is a valid number', () => {
        it('skips first n lists', (done) => {
          $request
            .end((err, res) => {
              expect(res.status).to.equal(200)
              expect(res).to.be.json
              expect(res.body).to.be.an('array').lengthOf(1)
              done()
            })
        })
      })
    })

    context('when limit=n is provided', () => {
      def('limit', () => 1)
      def('request', () => $server.get('/lists?limit=' + $limit))

      context('when limit is a valid number', () => {
        it('returns first n lists', (done) => {
          $request
            .end((err, res) => {
              expect(res.status).to.equal(200)
              expect(res).to.be.json
              expect(res.body).to.be.an('array').lengthOf(1)
              done()
            })
        })
      })

      context('when limit is not a valid number', () => {
        def('limit', () => 'aaa')

        it('returns 400', (done) => {
          $request
            .end((err, res) => {
              expect(res.status).to.equal(400)
              done()
            })
        })
      })
    })

    context('when multiple filters provided', () => {
      def('skip', () => 1)
      def('limit', () => 0)
      def('request', () => $server.get(`/lists?skip=${$skip}&limit=${$limit}`))

      it('filters lists', (done) => {
        $request
          .end((err, res) => {
            expect(res.status).to.equal(200)
            expect(res).to.be.json
            expect(res.body).to.be.an('array').lengthOf(0)
            done()
          })
      })
    })
  })

  describe('POST /lists', (done) => {
    def('id', () => 'd290f1ee-6c54-4b01-90e6-d701748f0852')
    def('name', () => 'name')
    def('request', () => {
      return $server
        .post('/lists')
        .send({ id: $id, name: $name })
    })

    context('when id is not a valid uuid', () => {
      def('id', () => 'aa')

      it('returns 400', (done) => {
        $request
          .end((err, res) => {
            expect(res.status).to.equal(400)
            done()
          })
      })
    })

    context('when id is a valid uuid', () => {
      context('when name is not provided', () => {
        def('name', () => void 0)

        it('returns 400', (done) => {
          $request
            .end((err, res) => {
              expect(res.status).to.equal(400)
              done()
            })
        })
      })

      context('when name is provided', () => {
        context('when list id already exists', () => {
          def('id', () => 'd290f1ee-6c54-4b01-90e6-d701748f0851')

          it('returns 409', (done) => {
            $request
              .end((err, res) => {
                expect(res.status).to.equal(409)
                done()
              })
          })
        })

        context('when list id does not exist', () => {
          it('returns 409', (done) => {
            $request
              .end((err, res) => {
                expect(res.status).to.equal(201)
                done()
              })
          })
        })
      })
    })
  })

  describe('GET /list/:id', (done) => {
    def('id', () => 'd290f1ee-6c54-4b01-90e6-d701748f0851')
    def('request', () => $server.get(`/list/${$id}`))

    context('when id is not a valid uuid', () => {
      def('id', () => 'aa')

      it('returns 400', (done) => {
        $request
          .end((err, res) => {
            expect(res.status).to.equal(400)
            done()
          })
      })
    })

    context('when id is a valid uuid', () => {
      context('when list with provided id not found', () => {
        def('id', () => 'd290f1ee-6c54-4b01-90e6-d701748f0855')

        it('returns 404', (done) => {
          $request
            .end((err, res) => {
              expect(res.status).to.equal(404)
              done()
            })
        })
      })

      context('when list with provided exists', () => {
        it('returns list', (done) => {
          $request
            .end((err, res) => {
              expect(res.status).to.equal(200)
              expect(res).to.be.json
              expect(res.body.id).to.equal($id)
              done()
            })
        })
      })
    })
  })
})