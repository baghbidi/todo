### Run on local machine

```
npm install
npm start
```
### Run in Docker container

```
docker-compose up
```
or
```
docker build -t todo .
docker run --rm -d -p 3000:3000 todo
```

### Run tests:
```
npm test
```
